<?php

namespace App\Http\Controllers;

use App\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['doctores']=Doctor::paginate(5);

        return view('doctor.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('doctor.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
            'especialidad'=>'required|string|max:100',
            'numCedula'=>'required|string|max:100',
            'imagen'=>'required|max:10000|mimes:jpeg,png,jpg',

        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        //$datosDoctor=request()->all();
        $datosDoctor=request()->except('_token');
        
        if($request->hasFile('imagen')){

            $datosDoctor['imagen']=$request->file('imagen')->store('uploads/doctores','public');
        }

        Doctor::insert($datosDoctor);

        //return response()->json($datosDoctor);
        return redirect('doctor')->with('Mensaje','Doctor Agregado con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $doctor = Doctor::findOrFail($id);
        return view("doctor.show", compact("doctor"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $doctor=Doctor::findOrFail($id);
        dd($doctor);
        return view('doctor.edit', compact('doctor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
            'especialidad'=>'required|string|max:100',
            'numCedula'=>'required|string|max:100',
        ];

        if($request->hasFile('imagen')){

            $campos+=['imagen'=>'required|max:10000|mimes:jpeg,png,jpg'];
        }

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosDoctor=request()->except(['_token','_method']);

        if($request->hasFile('imagen')){

            $doctor=Doctor::findOrFail($id);

            Storage::delete('public/'.$doctor->imagen);     

            $datosDoctor['imagen']=$request->file('imagen')->store('uploads/doctores','public'); 

        }

        Doctor::where('id','=',$id)->update($datosDoctor);

        //$doctor=Doctor::findOrFail($id);
        //return view('doctor.edit', compact('doctor'));
        return redirect('doctor')->with('Mensaje','Doctor Modificado con Exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        $doctor=Doctor::findOrFail($id);

        if(Storage::delete('public/'.$doctor->imagen)){

            Doctor::destroy($id);

        };

        return redirect('doctor')->with('Mensaje','Doctor Eliminado');
    }
}

