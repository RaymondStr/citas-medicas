<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinicas', function (Blueprint $table) {
            $table->id();
            /* $table->integer('doctor_id'); */
           /*  $table->integer('secretaria_id'); */
            $table->string('nombre');
            $table->string('domicilio');
            $table->string('calle');
            $table->string('codigoPostal');
            $table->string('fraccionamiento');
            $table->string('numero');
            $table->string('numeroIntrior');
            $table->string('telefono');
            $table->string('correoClinica');
            $table->string('imagen');
            $table->timestamps();
            
         /*    $table->foreign('doctor_id')->references('id')->on('doctors');
            $table->foreign('secretaria_id')->refenrences('id')->on('secretarias'); */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicas');
    }
}
