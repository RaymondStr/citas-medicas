<div class="form-group">

    <label for="nombre" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }}" name="nombre" id="nombre" 
    value="{{ isset($secretaria->nombre)?$secretaria->nombre:old('nombre') }}">
    {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
    

</div>

<div class="form-group">

    <label for="apellidoPaterno" class="control-label">{{'Apellido Paterno'}}</label>
    <input type="text" class="form-control {{ $errors->has('apellidoPaterno')?'is-invalid':'' }}" name="apellidoPaterno" id="apellidoPaterno" 
    value="{{ isset($secretaria->apellidoPaterno)?$secretaria->apellidoPaterno:old('apellidoPaterno') }}">
    {!! $errors->first('apellidoPaterno','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="apellidoMaterno" class="control-label">{{'Apellido Materno'}}</label>
    <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" 
    value="{{ isset($secretaria->apellidoMaterno)?$secretaria->apellidoMaterno:'' }}">
    
</div>

<div class="form-group">

    <label for="correo" class="control-label">{{'Correo'}}</label>
    <input type="email" class="form-control {{ $errors->has('correo')?'is-invalid':'' }}" name="correo" id="correo" 
    value="{{ isset($secretaria->correo)?$secretaria->correo:old('correo') }}">
    {!! $errors->first('correo','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="telefono" class="control-label">{{'Telefono'}}</label>
    <input type="text" class="form-control {{ $errors->has('telefono')?'is-invalid':'' }}" name="telefono" id="telefono" 
    value="{{ isset($secretaria->telefono)?$secretaria->telefono:old('telefono') }}">
    {!! $errors->first('telefono','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="domicilio" class="control-label">{{'Domicilio'}}</label>
    <input type="text" class="form-control {{ $errors->has('domicilio')?'is-invalid':'' }}" name="domicilio" id="domicilio" 
    value="{{ isset($secretaria->domicilio)?$secretaria->domicilio:old('domicilio') }}">
    {!! $errors->first('domicilio','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="contrasena" class="control-label">{{'Contraseña'}}</label>
    <input type="password" class="form-control {{ $errors->has('contrasena')?'is-invalid':'' }}" name="contrasena" id="contrasena" 
    value="{{ isset($secretaria->contrasena)?$secretaria->contrasena:'' }}">
    {!! $errors->first('contrasena','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
<a class="btn btn-primary" href="{{ url('secretaria') }}">Regresar</a>