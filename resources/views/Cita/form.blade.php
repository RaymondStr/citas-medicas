
<div class="form-group">

    <label for="fechaCita" class="control-label">{{'Fecha Cita'}}</label>
    <input type="date" class="form-control {{ $errors->has('fechaCita')?'is-invalid':'' }}" name="fechaCita" id="fechaCita" 
    value="{{ isset($cita->fechaCita)?$cita->fechaCita:old('fechaCita') }}">
    {!! $errors->first('fechaCita','<div class="invalid-feedback">:message</div>') !!}
    

</div>

<div class="form-group">

    <label for="horaCita" class="control-label">{{'Hora Cita'}}</label>
    <select class="form-control {{ $errors->has('horaCita')?'is-invalid':'' }}" name="horaCita" id="horaCita"
        value="{{ isset($cita->horaCita)?$cita->horaCita:old('horaCita') }}">
        <option value="">Selecione la Hora de su Cita</option>
        <option value="7">7:00 AM</option>
        <option value="8">8:00 AM</option>
        <option value="9">9:00 AM</option>
        <option value="10">10:00 AM</option>
        <option value="11">11:00 AM</option>
        <option value="12">12:00 PM</option>
        <option value="13">1:00 PM</option>
        <option value="14">2:00 PM</option>
        <option value="15">3:00 PM</option>
        <option value="16">4:00 PM</option>
        <option value="17">5:00 PM</option>
        <option value="18">6:00 PM</option>
        <option value="19">7:00 PM</option>
        <option value="20">8:00 PM</option>
        </select>
    {{-- <input type="text" class="form-control {{ $errors->has('horaCita')?'is-invalid':'' }}" name="horaCita" id="horaCita" 
    value="{{ isset($cita->horaCita)?$cita->horaCita:old('horaCita') }}"> --}}
    {!! $errors->first('horaCita','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
<a class="btn btn-primary" href="{{ url('cliente') }}">Regresar</a>