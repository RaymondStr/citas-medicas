
<div class="form-group">

    <label for="nombre" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }}" name="nombre" id="nombre" 
    value="{{ isset($doctor->nombre)?$doctor->nombre:old('nombre') }}">
    {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
    

</div>

<div class="form-group">

    <label for="apellidoPaterno" class="control-label">{{'Apellido Paterno'}}</label>
    <input type="text" class="form-control {{ $errors->has('apellidoPaterno')?'is-invalid':'' }}" name="apellidoPaterno" id="apellidoPaterno" 
    value="{{ isset($doctor->apellidoPaterno)?$doctor->apellidoPaterno:old('apellidoPaterno') }}">
    {!! $errors->first('apellidoPaterno','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="apellidoMaterno" class="control-label">{{'Apellido Materno'}}</label>
    <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" 
    value="{{ isset($doctor->apellidoMaterno)?$doctor->apellidoMaterno:'' }}">
    
</div>

<div class="form-group">

    <label for="correo" class="control-label">{{'Correo'}}</label>
    <input type="email" class="form-control {{ $errors->has('correo')?'is-invalid':'' }}" name="correo" id="correo" 
    value="{{ isset($doctor->correo)?$doctor->correo:old('correo') }}">
    {!! $errors->first('correo','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="telefono" class="control-label">{{'Telefono'}}</label>
    <input type="text" class="form-control {{ $errors->has('telefono')?'is-invalid':'' }}" name="telefono" id="telefono" 
    value="{{ isset($doctor->telefono)?$doctor->telefono:old('telefono') }}">
    {!! $errors->first('telefono','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="domicilio" class="control-label">{{'Domicilio'}}</label>
    <input type="text" class="form-control {{ $errors->has('domicilio')?'is-invalid':'' }}" name="domicilio" id="domicilio" 
    value="{{ isset($doctor->domicilio)?$doctor->domicilio:old('domicilio') }}">
    {!! $errors->first('domicilio','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="contrasena" class="control-label">{{'Contraseña'}}</label>
    <input type="password" class="form-control {{ $errors->has('contrasena')?'is-invalid':'' }}" name="contrasena" id="contrasena" 
    value="{{ isset($doctor->contrasena)?$doctor->contrasena:'' }}">
    {!! $errors->first('contrasena','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="especialidad" class="control-label">{{'Especialidad'}}</label>
    <input type="text" class="form-control {{ $errors->has('especialidad')?'is-invalid':'' }}" name="especialidad" id="especialidad" 
    value="{{ isset($doctor->especialidad)?$doctor->especialidad:old('especialidad') }}">
    {!! $errors->first('especialidad','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="numCedula" class="control-label">{{'Numero de Cedula'}}</label>
    <input type="text" class="form-control {{ $errors->has('numCedula')?'is-invalid':'' }}" name="numCedula" id="numCedula" 
    value="{{ isset($doctor->numCedula)?$doctor->numCedula:old('numCedula') }}">
    {!! $errors->first('numCedula','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="imagen" class="control-label">{{'Imagen'}}</label>
    @if(isset($doctor->imagen)) 
        <br>
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$doctor->imagen }}" alt="" width="100">
        <br>    
    @endif
    <input type="file" class="form-control {{ $errors->has('imagen')?'is-invalid':'' }}" name="imagen" id="imagen" value="">
    {!! $errors->first('imagen','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
<a class="btn btn-primary" href="{{ url('doctor') }}">Regresar</a>