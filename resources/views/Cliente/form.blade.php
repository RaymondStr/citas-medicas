
<div class="form-group">

    <label for="nombre" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }}" name="nombre" id="nombre" 
    value="{{ isset($cliente->nombre)?$cliente->nombre:old('nombre') }}">
    {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
    

</div>

<div class="form-group">

    <label for="apellidoPaterno" class="control-label">{{'Apellido Paterno'}}</label>
    <input type="text" class="form-control {{ $errors->has('apellidoPaterno')?'is-invalid':'' }}" name="apellidoPaterno" id="apellidoPaterno" 
    value="{{ isset($cliente->apellidoPaterno)?$cliente->apellidoPaterno:old('apellidoPaterno') }}">
    {!! $errors->first('apellidoPaterno','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="apellidoMaterno" class="control-label">{{'Apellido Materno'}}</label>
    <input type="text" class="form-control" name="apellidoMaterno" id="apellidoMaterno" 
    value="{{ isset($cliente->apellidoMaterno)?$cliente->apellidoMaterno:'' }}">
    
</div>

<div class="form-group">

    <label for="correo" class="control-label">{{'Correo'}}</label>
    <input type="email" class="form-control {{ $errors->has('correo')?'is-invalid':'' }}" name="correo" id="correo" 
    value="{{ isset($cliente->correo)?$cliente->correo:old('correo') }}">
    {!! $errors->first('correo','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="telefono" class="control-label">{{'Telefono'}}</label>
    <input type="text" class="form-control {{ $errors->has('telefono')?'is-invalid':'' }}" name="telefono" id="telefono" 
    value="{{ isset($cliente->telefono)?$cliente->telefono:old('telefono') }}">
    {!! $errors->first('telefono','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="domicilio" class="control-label">{{'Domicilio'}}</label>
    <input type="text" class="form-control {{ $errors->has('domicilio')?'is-invalid':'' }}" name="domicilio" id="domicilio" 
    value="{{ isset($cliente->domicilio)?$cliente->domicilio:old('domicilio') }}">
    {!! $errors->first('domicilio','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="contrasena" class="control-label">{{'Contraseña'}}</label>
    <input type="password" class="form-control {{ $errors->has('contrasena')?'is-invalid':'' }}" name="contrasena" id="contrasena" 
    value="{{ isset($cliente->contrasena)?$cliente->contrasena:'' }}">
    {!! $errors->first('contrasena','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="tipoSangre" class="control-label">{{'Tipo de Sangre'}}</label>
    <select class="form-control {{ $errors->has('tipoSangre')?'is-invalid':'' }}" name="tipoSangre" id="tipoSangre"
    value="{{ isset($cliente->tipoSangre)?$cliente->tipoSangre:old('tipoSangre') }}">
    <option value="">Selecione Su tipo de Sangre</option>
    <option value="A+">A+</option>
    <option value="B+">B+</option>
    <option value="O+">O+</option>
    <option value="AB+">AB+</option>
    <option value="A-">A-</option>
    <option value="B-">B-</option>
    <option value="O-">O-</option>
    <option value="AB-">AB-</option>
    </select>
    {{-- <input type="text" class="form-control {{ $errors->has('tipoSangre')?'is-invalid':'' }}" name="tipoSangre" id="tipoSangre" 
    value="{{ isset($cliente->tipoSangre)?$cliente->tipoSangre:old('tipoSangre') }}"> --}}
    {!! $errors->first('tipoSangre','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="enfermedadesCronicas" class="control-label">{{'Enfermedades Cronicas'}}</label>
    <input type="text" class="form-control {{ $errors->has('enfermedadesCronicas')?'is-invalid':'' }}" name="enfermedadesCronicas" id="enfermedadesCronicas" 
    value="{{ isset($cliente->enfermedadesCronicas)?$cliente->enfermedadesCronicas:old('enfermedadesCronicas') }}">
    {!! $errors->first('enfermedadesCronicas','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="alergias" class="control-label">{{'Alergias'}}</label>
    <input type="text" class="form-control {{ $errors->has('alergias')?'is-invalid':'' }}" name="alergias" id="alergias" 
    value="{{ isset($cliente->alergias)?$cliente->alergias:old('alergias') }}">
    {!! $errors->first('alergias','<div class="invalid-feedback">:message</div>') !!}
    
</div>


<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
<a class="btn btn-primary" href="{{ url('cliente') }}">Regresar</a>