@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

{{-- <a href="{{ url('cita/create') }}" class="btn btn-success">Agregar Secretaria</a>
<br><br> --}}

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Nombre</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Domicilio</th>
           <th>Contraseña</th>
           <th>Tipo de Sangre</th>
           <th>Enfermedad Cronica</th>
           <th>Alergias</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       <tr>        
           <td>{{ $cliente->nombre }} {{ $cliente->apellidoPaterno }} {{ $cliente->apellidoMaterno }}</td>
           <td>{{ $cliente->correo }}</td>
           <td>{{ $cliente->telefono }}</td>
           <td>{{ $cliente->domicilio }}</td>
           <td>{{ $cliente->contrasena }}</td>
           <td>{{ $cliente->tipoSangre }}</td>
           <td>{{ $cliente->enfermedadesCronicas }}</td>
           <td>{{ $cliente->alergias }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/cliente/'.$cliente->id.'/edit') }}">
               Editar
               </a>
           </td>
       </tr>
       
   </tbody>
</table>
</div>
@endsection