<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    //
    protected $fillable = [
        'nombre', 'domicilio', 'telefono', 'correoClinica', 'imagen'
    ];

    public function clinica()
    {
        return $this->belongsTo(Doctor::class);
    }
    
}
