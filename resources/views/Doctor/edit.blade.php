
@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{ url('/doctor/' .$doctor->id) }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}
 {{ method_field('PATCH') }}   

 @include('doctor.form',['Modo'=>'editar'])

</form>
</div>
@endsection