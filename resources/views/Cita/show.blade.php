@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('cita/create') }}" class="btn btn-success">Agregar Cita</a>
<br><br> 

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Fecha Cita</th>
           <th>Hora Cita</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       <tr>        
           <td>{{ $cita->fechaCita }}</td>
           <td>{{ $cita->horaCita }}</td>
          
           <td>
               <a class="btn btn-warning" href="{{ url('/cita/'.$cita->id.'/edit') }}">
               Editar
               </a>
           </td>
       </tr>
       
   </tbody>
</table>
</div>
@endsection