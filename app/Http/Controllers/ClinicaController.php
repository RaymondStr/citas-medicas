<?php

namespace App\Http\Controllers;

use App\Clinica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['clinicas']=Clinica::paginate(5);

        return view('clinica.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clinica.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'telefono'=>'required|string|max:100',
            'correoClinica'=>'required|email',
            'imagen'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
        

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

       
        $datosClinica=request()->except('_token');
        
        if($request->hasFile('imagenClinica')){

            $datosClinica['imagenClinica']=$request->file('imagenClinica')->store('uploads/clinicas','public');
        }

        Doctor::insert($datosClinica);

        return redirect('clinica')->with('Mensaje','Clinica Agregada con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function show(Clinica $clinica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $clinica=Clinica::findOrFail($id);

        return view('clinica.edit', compact('clinica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'telefono'=>'required|string|max:100',
            'correoClinica'=>'required|email',
            'imagen'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];

        if($request->hasFile('imagenClinica')){

            $campos+=['imagenClinica'=>'required|max:10000|mimes:jpeg,png,jpg'];
        }

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosDoctor=request()->except(['_token','_method']);

        if($request->hasFile('imagenClinica')){

            $clinica=Clinica::findOrFail($id);

            Storage::delete('public/'.$clinica->imagenClinica);     

            $datosClinica['imagenClinica']=$request->file('imagenClinica')->store('uploads/clinicas','public'); 

        }

        Clinica::where('id','=',$id)->update($datosClinica);

        return redirect('clinica')->with('Mensaje','Clinica Modificada con Exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $clinica=Clinica::findOrFail($id);

        if(Storage::delete('public/'.$clinica->imagenClinica)){

            Clinica::destroy($id);

        };

        return redirect('clinica')->with('Mensaje','Clinica Eliminada');
    }
}
