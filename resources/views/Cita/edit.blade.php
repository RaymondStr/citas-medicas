
@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{ route('cita/' .$cita->id) }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}
 {{ method_field('PATCH') }}   

 @include('cita.form',['Modo'=>'editar'])

</form>
</div>
@endsection