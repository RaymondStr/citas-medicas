@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>
@php
    $doctorId = Auth::id();
    $email    = Auth::user()->email;
@endphp
@endif
<br><br>

<a href="{{ url('doctor/create') }}" class="btn btn-success">Agregar Doctor</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Imagen</th>
           <th>Nombre</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Domicilio</th>
           <th>Contraseña</th>
           <th>Especialidad</th>
           <th>Numero de Cedula</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       @foreach ($doctores as $doctor)
       <tr> 
           <td>{{ $loop->iteration }}</td>         
           <td>
               
            
               <img src="{{ asset('storage').'/'.$doctor->imagen }}" class="img-thumbnail img-fluid" alt="" width="100">
           </td>
           <td>{{ $doctor->nombre }} {{ $doctor->apellidoPaterno }} {{ $doctor->apellidoMaterno }}</td>
           <td>{{ $doctor->correo }}</td>
           <td>{{ $doctor->telefono }}</td>
           <td>{{ $doctor->domicilio }}</td>
           <td>{{ $doctor->contrasena }}</td>
           <td>{{ $doctor->especialidad }}</td>
           <td>{{ $doctor->numCedula }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/doctor/'.$doctor->id.'/edit') }}">
               Editar
               </a>

               <form method="post" action="{{ url('/doctor/'.$doctor->id) }}" style="display:inline">
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
               </form>
           </td>
       </tr>
       @endforeach 
       
   </tbody>
</table>
{{ $doctores->links() }}
</div>
@endsection