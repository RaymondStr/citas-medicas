<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Secretaria extends Model
{
    //
    protected $guard = 'secretaria';
    
    protected $fillable = [
        'nombre', 'apellidoPaterno', 'apellidoMaterno', 'correo',
         'telefono', 'domicilio', 'contrasena'
    ];

    protected $hidden = [
        'contrasena', 'remember_token',
    ];
}
