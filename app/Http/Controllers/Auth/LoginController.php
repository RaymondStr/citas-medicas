<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
            $this->middleware('guest')->except('logout');
            $this->middleware('guest:doctor')->except('logout');
            $this->middleware('guest:cliente')->except('logout');
            $this->middleware('guest:secretaria')->except('logout');
    }

     public function showDoctorLoginForm()
    {
        return view('auth.login', ['url' => 'doctor']);
    }

    public function doctorLogin(Request $request)
    {
        $this->validate($request, [
            'correo'   => 'required|email',
            'conttrasena' => 'required|min:6'
        ]);

        if (Auth::guard('doctor')->attempt(['correo' => $request->correo, 'contrasena' => $request->contrasena], $request->get('remember'))) {

            return redirect()->intended('/doctor');
        }
        return back()->withInput($request->only('correo', 'remember'));
    }

     public function showClienteLoginForm()
    {
        return view('auth.login', ['url' => 'cliente']);
    }

    public function clienteLogin(Request $request)
    {
        $this->validate($request, [
            'correo'   => 'required|email',
            'contrasena' => 'required|min:6'
        ]);

        if (Auth::guard('cliente')->attempt(['correo' => $request->correo, 'contrasena' => $request->contrasena], $request->get('remember'))) {

            return redirect()->intended('/cliente');
        }
        return back()->withInput($request->only('correo', 'remember'));

    }

    public function showSecretariaLoginForm()
    {
        return view('auth.login', ['url' => 'secretaria']);
    }

    public function secretariaLogin(Request $request)
    {
        $this->validate($request, [
            'correo'   => 'required|email',
            'conttrasena' => 'required|min:6'
        ]);

        if (Auth::guard('secretaria')->attempt(['correo' => $request->correo, 'contrasena' => $request->contrasena], $request->get('remember'))) {

            return redirect()->intended('/secretaria');
        }
        return back()->withInput($request->only('correo', 'remember'));
    }
}