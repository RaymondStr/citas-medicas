
<div class="form-group">

    <label for="nombre" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }}" name="nombre" id="nombre" 
    value="{{ isset($clinica->nombre)?$clinica->nombre:old('nombre') }}">
    {!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
    

</div>

<div class="form-group">

    <label for="domicilio" class="control-label">{{'Domicilio'}}</label>
    <input type="text" class="form-control {{ $errors->has('domicilio')?'is-invalid':'' }}" name="domicilio" id="domicilio" 
    value="{{ isset($clinica->domicilio)?$clinica->domicilio:old('domicilio') }}">
    {!! $errors->first('domicilio','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="telefono" class="control-label">{{'Telefono'}}</label>
    <input type="text" class="form-control {{ $errors->has('telefono')?'is-invalid':'' }}" name="telefono" id="telefono" 
    value="{{ isset($clinica->telefono)?$clinica->telefono:old('telefono') }}">
    {!! $errors->first('telefono','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<div class="form-group">

    <label for="correo" class="control-label">{{'Correo'}}</label>
    <input type="email" class="form-control {{ $errors->has('correo')?'is-invalid':'' }}" name="correo" id="correo" 
    value="{{ isset($clinica->correo)?$clinica->correo:old('correo') }}">
    {!! $errors->first('correo','<div class="invalid-feedback">:message</div>') !!}
    
</div>


<div class="form-group">

    <label for="imagenClinica" class="control-label">{{'Imagen'}}</label>
    @if(isset($clinica->imagenClinica)) 
        <br>
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$clinica->imagenClinica }}" alt="" width="100">
        <br>    
    @endif
    <input type="file" class="form-control {{ $errors->has('imagenClinica')?'is-invalid':'' }}" name="imagenClinica" id="imagenClinica" value="">
    {!! $errors->first('imagenClinica','<div class="invalid-feedback">:message</div>') !!}
    
</div>

<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
<a class="btn btn-primary" href="{{ url('clinica') }}">Regresar</a>