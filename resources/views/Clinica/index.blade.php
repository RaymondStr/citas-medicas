@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('clinica/create') }}" class="btn btn-success">Agregar Clinica</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Imagen</th>
           <th>Nombre</th>
           <th>Domicilio</th>
           <th>Telefono</th>
           <th>Correo Clinica</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       @foreach ($clinicas as $clinica)
       <tr> 
           <td>{{ $loop->iteration }}</td>         
           <td>
               <img src="{{ asset('storage').'/'.$clinica->imagenClinica }}" class="img-thumbnail img-fluid" alt="" width="100">
           </td>
           <td>{{ $clinica->nombre }}</td>
           <td>{{ $clinica->domicilio }}</td>
           <td>{{ $clinica->telefono }}</td>
           <td>{{ $clinica->correo }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/clinica/'.$clinica->id.'/edit') }}">
               Editar
               </a>

               <form method="post" action="{{ url('/clinica/'.$clinica->id) }}" style="display:inline">
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
               </form>
           </td>
       </tr>
       @endforeach 
       
   </tbody>
</table>
{{ $clinicas->links() }}
</div>
@endsection