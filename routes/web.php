<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

///con esta ruta hacemos que poniendo el url de la web no le de acceso hasta no/// 
///estar logeado poniendo la instruccion " ->middleware('auth') " despues de la ruta///
//Route::resource('doctor', 'DoctorController')->middleware('auth');

///Con esta ruta no esta la autenticacion y si solo ponemos la url podemos entrar 
///a explorar sin necesidad de estar logeado
Route::resource('doctor', 'DoctorController');

Route::resource('cliente', 'ClienteController');

Route::resource('secretaria', 'SecretariaController');

Route::resource('clinica', 'ClinicaController');

Route::resource('cita', 'CitaController');

Auth::routes(['reset'=>false]);
//'register'=>false,


Auth::routes();

Route::get('/login/doctor', 'Auth\LoginController@showDoctorLoginForm');
Route::get('/login/cliente', 'Auth\LoginController@showClienteLoginForm');
Route::get('/login/secretaria', 'Auth\LoginController@showSecretariaLoginForm');
Route::get('/register/doctor', 'Auth\RegisterController@showDoctorRegisterForm');
Route::get('/register/cliente', 'Auth\RegisterController@showClienteRegisterForm');
Route::get('/register/secretaria', 'Auth\RegisterController@showSecretariaRegisterForm');

Route::post('/login/doctor', 'Auth\LoginController@DoctorLogin');
Route::post('/login/cliente', 'Auth\LoginController@ClienteLogin');
Route::post('/login/secretaria', 'Auth\LoginController@SecretariaLogin');
Route::post('/register/doctor', 'Auth\RegisterController@createDoctor');
Route::post('/register/cliente', 'Auth\RegisterController@createCliente');
Route::post('/register/secretaria', 'Auth\RegisterController@createSecretaria');

Route::view('/home', 'home')->middleware('auth');
Route::view('/doctor', 'doctor');
Route::view('/cliente', 'cliente');
Route::view('/secretaria', 'secretaria');
