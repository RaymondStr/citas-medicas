@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('secretaria/create') }}" class="btn btn-success">Agregar Secretaria</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Imagen</th>
           <th>Nombre</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Domicilio</th>
           <th>Contraseña</th>
       </tr>
   </thead>
   
   <tbody>
       @foreach ($secretarias as $secretaria)
       <tr> 
           <td>{{ $loop->iteration }}</td>         
           <td>{{ $secretaria->nombre }} {{ $secretaria->apellidoPaterno }} {{ $secretaria->apellidoMaterno }}</td>
           <td>{{ $secretaria->correo }}</td>
           <td>{{ $secretaria->telefono }}</td>
           <td>{{ $secretaria->domicilio }}</td>
           <td>{{ $secretaria->contrasena }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/secretaria/'.$secretaria->id.'/edit') }}">
               Editar
               </a>

               <form method="post" action="{{ url('/secretaria/'.$secretaria->id) }}" style="display:inline">
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
               </form>
           </td>
       </tr>
       @endforeach 
       
   </tbody>
</table>
{{ $secretarias->links() }}
</div>
@endsection