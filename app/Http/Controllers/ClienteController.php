<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['clientes']=Cliente::paginate(5);

        return view('cliente.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
            'tipoSangre'=>'required|string|max:100',
            'alergias'=>'required|string|max:100',
            'enfermedadesCronicas'=>'required|string|max:100',

        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosCliente=request()->except('_token');

        Cliente::insert($datosCliente);

        return redirect('cliente')->with('Mensaje','Cliente Agregado con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cliente = Cliente::findOrFail($id);
        return view("cliente.show", compact("cliente"));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente=Cliente::findOrFail($id);

        return view('cliente.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
            'tipoSangre'=>'required|string|max:100',
            'alergias'=>'required|string|max:100',
            'enfermedadesCronicas'=>'required|string|max:100',
        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosCliente=request()->except(['_token','_method']);

        Cliente::where('id','=',$id)->update($datosCliente);

        return redirect('cliente')->with('Mensaje','Cliente Modificado con Exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cliente=Cliente::findOrFail($id);

        Cliente::destroy($id);

        return redirect('cliente')->with('Mensaje','Cliente Eliminado');
    }
}
