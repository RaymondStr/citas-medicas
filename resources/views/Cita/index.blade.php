@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('cita/create') }}" class="btn btn-success">Agregar Cita</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Fecha Cita</th>
           <th>Hora Cita</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       @foreach ($citas as $cita)
       <tr> 
           <td>{{ $loop->iteration }}</td>         
           <td>{{ $cita->fechaCita }}</td>
           <td>{{ $cita->horaCita }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/cita/'.$cita->id.'/edit') }}">
               Editar
               </a>

               <form method="post" action="{{ url('/cita/'.$cita->id) }}" style="display:inline">
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
               </form>
           </td>
       </tr>
       @endforeach 
       
   </tbody>
</table>
{{ $citas->links() }}
</div>
@endsection