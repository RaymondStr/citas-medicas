<?php

namespace App\Http\Controllers;

use App\Secretaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SecretariaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['secretarias']=Secretaria::paginate(5);

        return view('secretaria.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('secretaria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosSecretaria=request()->except('_token');

        Secretaria::insert($datosSecretaria);

        return redirect('secretaria')->with('Mensaje','Secretaria Agregado con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secretaria  $secretaria
     * @return \Illuminate\Http\Response
     */
    public function show(Secretaria $secretaria)
    {
        //
        $secretaria = Secretaria::findOrFail($id);
        return view("secretaria.show", compact("secretaria"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secretaria  $secretaria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente=Secretaria::findOrFail($id);

        return view('secretaria.edit', compact('secretaria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secretaria  $secretaria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre'=>'required|string|max:100',
            'apellidoPaterno'=>'required|string|max:100',
            'correo'=>'required|email',
            'telefono'=>'required|string|max:100',
            'domicilio'=>'required|string|max:100',
            'contrasena'=>'required|string|max:100',
        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosSecreetaria=request()->except(['_token','_method']);

        Secretaria::where('id','=',$id)->update($datosSecretaria);

        return redirect('secretaria')->with('Mensaje','Secretaria Modificado con Exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secretaria  $secretaria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $secretaria=Secretaria::findOrFail($id);

        Secretaria::destroy($id);

        return redirect('secretaria')->with('Mensaje','Secretaria Eliminada');
    }
}
