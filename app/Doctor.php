<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Model
{
    //
    protected $guard = 'doctor';

    protected $fillable = [
        'nombre', 'apellidoPaterno', 'apellidoMaterno', 'correo', 'telefono', 
        'domicilio', 'contrasena', 'especialidad', 'numCedula', 'imagen'
    ];

    protected $hidden = [
        'contrasena', 'remember_token',
    ];

   public function clinica()
   {
       return $this->hasOne(Clinica::class);
   }
}
