<?php

namespace App\Http\Controllers;

use App\Cita;
use Illuminate\Http\Request;

class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['citas']=Cita::paginate(5);

        return view('cita.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'fechaCita'=>'required|string|max:100',
            'horaCita'=>'required|string|max:100',
        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosCita=request()->except('_token');

        Cita::insert($datosCita);

        return redirect('cita')->with('Mensaje','Cita Agregada con Exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cita = Cita::findOrFail($id);
        return view("cita.show", compact("cita"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cita=Cita::findOrFail($id);

        return view('cita.edit', compact('cita'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'fechaCita'=>'required|date_format:Y-m-d',
            'horaCita'=>'required|string|max:100',
        ];

        $Mensaje=["required"=>"El :attribute es requerido"];

        $this->validate($request,$campos,$Mensaje);

        $datosCita=request()->except(['_token','_method']);

        Cita::where('id','=',$id)->update($datosCita);

        return redirect('cita')->with('Mensaje','Cita Modificada con Exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cita=Cita::findOrFail($id);

        Cita::destroy($id);

        return redirect('cita')->with('Mensaje','Cita Eliminada');
    }
}
