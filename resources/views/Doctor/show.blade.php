@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('secretaria/create') }}" class="btn btn-success">Agregar Secretaria</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Imagen</th>
           <th>Nombre</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Domicilio</th>
           <th>Contraseña</th>
           <th>Especialidad</th>
           <th>Numero de Cedula</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       <tr>        
           <td>
               <img src="{{ asset('storage').'/'.$doctor->imagen }}" class="img-thumbnail img-fluid" alt="" width="100">
           </td>
           <td>{{ $doctor->nombre }} {{ $doctor->apellidoPaterno }} {{ $doctor->apellidoMaterno }}</td>
           <td>{{ $doctor->correo }}</td>
           <td>{{ $doctor->telefono }}</td>
           <td>{{ $doctor->domicilio }}</td>
           <td>{{ $doctor->contrasena }}</td>
           <td>{{ $doctor->especialidad }}</td>
           <td>{{ $doctor->numCedula }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/doctor/'.$doctor->id.'/edit') }}">
               Editar
               </a>
           </td>
       </tr>
       
   </tbody>
</table>
</div>
@endsection