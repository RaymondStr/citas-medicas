
@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{ url('/cliente/' .$cliente->id) }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}
 {{ method_field('PATCH') }}   

 @include('cliente.form',['Modo'=>'editar'])

</form>
</div>
@endsection