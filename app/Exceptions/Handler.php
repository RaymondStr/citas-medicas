<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Auth;
class Handler extends ExceptionHandler
{
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        if ($request->is('doctor') || $request->is('doctor/*')) {
            return redirect()->guest('/login/doctor');
        }
        if ($request->is('cliente') || $request->is('cliente/*')) {
            return redirect()->guest('/login/cliente');
        }
        if ($request->is('secretaria') || $request->is('secretaria/*')) {
            return redirect()->guest('/login/secretaria');
        }
        return redirect()->guest(route('login'));
    }
}

