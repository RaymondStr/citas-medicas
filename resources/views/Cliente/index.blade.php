@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
    {{  Session::get('Mensaje')  }}
</div>

@endif
<br><br>

<a href="{{ url('cliente/create') }}" class="btn btn-success">Agregar Cliente</a>
<br><br>

<table class="table-dark table-hover">

   <thead class="table-dark">
       <tr>
           <th>#</th>
           <th>Nombre</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Domicilio</th>
           <th>Contraseña</th>
           <th>Tipo de Sangre</th>
           <th>Enfermedad Cronica</th>
           <th>Alergias</th>
           <th>Acciones</th>
       </tr>
   </thead>
   
   <tbody>
       @foreach ($clientes as $cliente)
       <tr> 
           <td>{{ $loop->iteration }}</td>         
           <td>{{ $cliente->nombre }} {{ $cliente->apellidoPaterno }} {{ $cliente->apellidoMaterno }}</td>
           <td>{{ $cliente->correo }}</td>
           <td>{{ $cliente->telefono }}</td>
           <td>{{ $cliente->domicilio }}</td>
           <td>{{ $cliente->contrasena }}</td>
           <td>{{ $cliente->tipoSangre }}</td>
           <td>{{ $cliente->enfermedadesCronicas }}</td>
           <td>{{ $cliente->alergias }}</td>
           <td>
               <a class="btn btn-warning" href="{{ url('/cliente/'.$cliente->id.'/edit') }}">
               Editar
               </a>

               <form method="post" action="{{ url('/cliente/'.$cliente->id) }}" style="display:inline">
               {{ csrf_field() }}
               {{ method_field('DELETE') }}
               <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
               </form>
           </td>
       </tr>
       @endforeach 
       
   </tbody>
</table>
{{ $clientes->links() }}
</div>
@endsection