<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Model
{
    //
    protected $guard = 'cliente';

    protected $fillable = [
        'nombre', 'apellidoPaterno', 'apellidoMaterno', 'correo', 'telefono', 
        'domicilio', 'contrasena', 'tipoSangre', 'alergias', 
        'enfermedadesCronicas'  
    ];

    protected $hidden = [
        'contrasena', 'remember_token',
    ];
}
