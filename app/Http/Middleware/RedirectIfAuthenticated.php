<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ($guard == "doctor" && Auth::guard($guard)->check()) {
            return redirect('/doctor');
        }
        if ($guard == "cliente" && Auth::guard($guard)->check()) {
            return redirect('/cliente');
        }
        if ($guard == "secretaria" && Auth::guard($guard)->check()) {
            return redirect('/secretaria');
        }
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    }
}
