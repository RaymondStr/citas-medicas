
@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{ url('/clinica/' .$clinica->id) }}" method="post" enctype="multipart/form-data">
 {{ csrf_field() }}
 {{ method_field('PATCH') }}   

 @include('clinica.form',['Modo'=>'editar'])

</form>
</div>
@endsection